<?php

/**
 * @file
 * Contains \Drupal\always_visible\AlwaysVisibleServiceProvider.
 */

namespace Drupal\always_visible;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Class MyModuleServiceProvider.
 */
class AlwaysVisibleServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('menu.link_tree');
    $definition->setClass('Drupal\always_visible\Menu\MenuLinkTree');
  }

}
