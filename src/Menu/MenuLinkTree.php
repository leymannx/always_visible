<?php

/**
 * @file
 * Contains \Drupal\always_visible\Menu\MenuLinkTree.
 */

namespace Drupal\always_visible\Menu;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Menu\MenuLinkTree as MenuLinkTreeCore;

/**
 * Override \Drupal\Core\Menu\MenuLinkTree.
 *
 * @see \Drupal\always_visible\AlwaysVisibleServiceProvider::alter()
 */
class MenuLinkTree extends MenuLinkTreeCore {

  /**
   * {@inheritdoc}
   */
  protected function createInstances(array $data_tree) {
    $tree = parent::createInstances($data_tree);
    foreach ($tree as $key => $element) {
      $tree[$key]->access = AccessResult::allowed();
    }
    return $tree;
  }

}
